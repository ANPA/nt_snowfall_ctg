## nt_snowfall_ctg

- Please note that the master branch is also the working branch; if you want to access a specific earlier beta version state, see the [repository tags](https://gitlab.com/ANPA/nt_snowfall_ctg/-/tags).
- BSP release is available at [GameBanana](https://gamebanana.com/mods/141026)
- Changelog thread is available at [NT architects](https://steamcommunity.com/groups/NTarchitects/discussions/0/1694920442950672196/)

### Map overview:

<img src="https://gitlab.com/ANPA/nt_snowfall_ctg/-/raw/master/promo/snowfall_b12_unmarked_plain.jpg" alt="level overview of version beta 12" width="750px" />
